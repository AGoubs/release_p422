﻿class log {
    static success = msg => {
        console.log('%c' + msg, 'color: #080');
    }
    static error = msg => {
        console.error(msg);
    }
}

class broker {
    static count = 0;
    static connection;

    static connect() {
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl("/broker")
            .build();
        this.connection.on("Request", function (message) {
        });
        this.connection.on("Response", function (message) {
            const parsedData = jQuery.parseJSON(message);
            const additionalData = parsedData.features[0].properties.summary;

            $("#duree").text(Math.round(additionalData.duration / 60) + "min");
            $("#distance").text(getDistance(additionalData.distance));

            $('#icon-to-change').removeClass();
            if (parsedData.metadata.query.profile === "driving-car") {
                $('#icon-to-change').addClass("fas fa-car fa-2x");
            }
            else if (parsedData.metadata.query.profile === "cycling-regular") {
                $('#icon-to-change').addClass("fas fa-biking fa-2x");
            }
            else if (parsedData.metadata.query.profile === "foot-walking") {
                $('#icon-to-change').addClass("fas fa-hiking fa-2x");
            }
            else {
                $('#icon-to-change').addClass("fas fa-road fa-2x");
            }

            sendAjax("carroute");

            $(".leaflet-zoom-animated g .leaflet-interactive").remove();
            L.geoJSON(jQuery.parseJSON(message)).addTo(mymap);
        });
        this.connection.start().catch(function (err) {
            return console.error(err.toString());
        });
    }

    static updateState() {
        return;
    }

    static emit(coords) {
        this.connection.invoke("Request", JSON.stringify(coords)).catch(function (err) {
            return console.error(err.toString());
        });
    }

    static test(e) {
        e.preventDefault();
        let testCoords = {};
        testCoords.startlat = 45.7505926;
        testCoords.startLong = 4.8113262;
        testCoords.stopLat = 45.7380158;
        testCoords.stopLong = 4.8680768;
        testCoords.TransportType = "all";
        this.connection.invoke("Request", JSON.stringify(testCoords)).catch(function (err) {
            return console.error(err.toString());
        });
        return false;
    }
}

function getDistance(distance) {
    if (distance < 1000) {
        return distance.toFixed(0) + "m";
    } else {
        return (distance / 1000).toFixed(1) + "km";
    }
}


$(document).ready(_ => {
    broker.connect();
    $(document).on('click', '#test', function (e) {
        e.preventDefault();

        broker.test(e);

        $(this).hide();
        setTimeout(_ => {
            $(this).show();
        }, 1000);
    });
});

function sendAjax2(url) {
    $.ajax({
        type: "POST",
        data: JSON.stringify(Coords),
        url: url,
        contentType: "application/json",
        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
    }).done(function (data) {
        const parsedData = jQuery.parseJSON(data);
        const coords = parsedData.features[0].bbox;
        const extraData = {
            metrics: ["duration"],
            locations: [[coords[0], coords[1]], [coords[2], coords[3]]]
        };

        getAllDuration2(JSON.stringify(extraData), url);

    }).fail(function (e) { console.log(e); });
}

$(".leaflet-locationiq-close").on("click", function () {
    window.$(".leaflet-zoom-animated g .leaflet-interactive").remove();
});

function getAllDuration2(coords, url) {
    $.ajax({
        url: 'https://api.openrouteservice.org/v2/matrix/driving-car?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
        type: 'POST',
        data: coords,
        contentType: "application/json",
        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
    }).done(function (otherData) {
        $("#carTime").text(Math.round(otherData.durations[1][0] / 60) + " min");
    }).fail(function (e) { console.log(e); });

    $.ajax({
        url: 'https://api.openrouteservice.org/v2/matrix/cycling-regular?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
        type: 'POST',
        data: coords,
        contentType: "application/json",
        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
    }).done(function (otherData) {
        $("#cyclingTime").text(Math.round(otherData.durations[1][0] / 60) + " min");
    }).fail(function (e) { console.log(e); });

    $.ajax({
        url: 'https://api.openrouteservice.org/v2/matrix/foot-walking?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
        type: 'POST',
        data: coords,
        contentType: "application/json",
        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
    }).done(function (otherData) {
        $("#footTime").text(Math.round(otherData.durations[1][0] / 60) + " min");

    }).fail(function (e) { console.log(e); });
}